PA_212888_7-15MarloweWay.txt v1.0.0 2021/11/15

Convenient secure cycle parking should be provided at one space per bedroom 
that is as convenient as the car garages [1].  

NB The use of car garages for cycle parking is not advised because any space 
left over is rarely suitable.  Many cycle lockers and hangars are available, 
c.f. Cyclehoop [2].  The Dutch commonly provide space in utility rooms.

Colchester Cycling Campaign

Note for CBC censor: Please only censor lines with links, and not links with 
*.gov.uk domains.

[1] Local Transport Note 1/20 (LTN 1/20), DfT, 2020
https://www.gov.uk/government/publications/cycle-infrastructure-design-ltn-120
[2] Cyclehoop ``We create innovative cycle parking and infrastructure.''
https://www.cyclehoop.com/category/home-storage/
