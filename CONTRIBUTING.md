# Contributing
## Claiming an application and assembling a response
1. If you see in a roundup or elsewhere an application that you'd like to tackle, let me know through the members' mailing list.  Reply to the relevant roundup but add distinctive information to the subject line, like ''... [app. no.] CatchbellsLondonRoad''
2. Make a note of the consultation [end] date
3. When you're ready (for feedback, or to submit), send round a draft to the members' mailing list using the same thread as previously.  If the file is large (>100MiB), add it to your preferred file host (or Ensembling) and send a link instead.  When sending a draft for submission, do so at least three days before the consultation deadline to give everyone a chance to have seen it

## Submitting a response
1. Submit to CBC, or whomever, via
	* webform.  The CBC webform only accepts plain text.  If you would like to submit in any other format, you must use email.
	* email.  For CBC, use, at minimum, the following email headers:        
	To: planning.services@..., case.officer@...     
	Subject: [Application Number] ([Case Officer])       
	* Monday is a good day to aim to submit a response because applications are grouped in `PA_triaged.csv` by week, starting on Mondays
2. Forward/cc to interested parties
	* Additionally, you may wish to copy in/forward to: the parish council, ward CBC councillors, division ECC councillor, CBC transport officers Rachel Forkin and Jane Thompson, ECC Highways Planning officer Martin Mason, ECC Sustainable Transport officer Tracey Vickers, and the CCC Members' email list
	* All officer addresses are firstname.lastname@blah.gov.uk, where blah is ''colchester'' for CBC and ''essex'' for ECC			     
3. Add the response to our archive
  	1. Add the document to Ensembling, in the [Pubished Planning Responses](https://www.ensembling.com/264857) directory as PA_[application no.]\_\[short label\]\_\[response no.\].\[extension\], c.f. other entries (PA for Planning Application, PC for Consultations, TRO etc.)
  	2. Then create an [issue](https://colchester.cyclescape.org/issues/new) on Cyclescape with a link to the CBC application page in the url field, tags: "planning, cbc, current", and put the link to the response on Ensembling in a post on a \[sub-\]thread.
c.f. this [example](https://colchester.cyclescape.org/issues/4592-pa_212888_7-15marloweway), or [this](https://colchester.cyclescape.org/issues/4609-pa_212989-212990_homefrmcotta133) that combines two related applications into one issue.
4. Let me know, or, if you are feeling adventurous, you might also like to submit a patch to update [PA_triaged.csv](PA_triaged.csv)

## Maintenance
### Weekly Procedure
1. Through the week
	* look out for updates/decisions/etc., and date/agenda for next Plannig Cttee.
2. On Friday or over the weekend:
	1. Check claimed: follow up any claimed but, as yet, unresponded to applications
	2. Check triaged: claim and draft responses to any applications whose consultation dates fall before next week, and look ahead to larger applications that will take longer.  Send out draft responses to the members' mailing list as a reply to the roundup that introduced them, with identifier in the subject line, for consideration before submission
	3. Check for new applications of interest
		1. Look through [planning-search-results](https://www.colchester.gov.uk/planning-search-results/) to add all new applications with ''Date Registered'' within the last four weeks to `PA_list_CBC.csv` and 'note' any interesting ones with ''noted'' in the status field
		2. run `make triage` (or equivalent) to move the noted applications to `PA_triaged.csv` and fill in remaining data: consultation and target dates, and link to council webpage (keep the browser tabs open and close them as you fill the information in).  Sort the new applications under ''triaged'' by consultation date
	4. write the weekly roundup
3. Monday/submission day
	* submit the response through the website or via email, as above

### Medium term
* Check applications of interest: have new documents been added, substantial changes made?

### Longer term
* Clean up all applications: changed dates/status/etc.

