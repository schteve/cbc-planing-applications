# CBC Planning Applications
## Quick Links
* [issues map](https://issuemap.cyclescape.org/index.html?title=Colchester%20CC&center=51.895,0.885&zoom=13&bbox=0.5,51.5,1.25,52&info=https://colchester.cyclescape.org/&mainmenu=Success:success*/Current:current*/ALL&secondmenu=none&showexpired=true&showstale=true&filtertag1=none)
* published responses in
	* [geographical threads](https://colchester.cyclescape.org/tags/planning)
	* [a regular directory](https://www.ensembling.com/264857)\*
* [README](README.md): This README
* [CONTRIBUTING](CONTRIBUTING.md): How to help out
* files:
	* [PA_triaged.csv](PA_triaged.csv): Applications of interest (Current state of play)
	* [PC_list.csv](PC_list.csv): All consultations
	* [PA_list_CBC.csv](PA_list_CBC.csv): All CBC planning applications
	* [PA_list_ECC.csv](PA_list_ECC.csv): ECC planning applications

\*[sign up to view](https://www.ensembling.com/264942?k=qzvkZy)

## Motivation
A good deal of this infrastructure would be unnecessary if CBC's systems provided more automation, or read-access to third-parties, e.g. [planit.org.uk](https://planit.org.uk).

## Notes
First of all, ''Comma Separated Values'' (csv) was chosen as a simple format that is accesible via text editors, like [vim](https://www.vim.org/), as well as spreadsheet software, and is trackable via [git](https://git-scm.com/).

* `PA_list_CBC.csv` contains all applications from CBC since some start point (~March 2021) and contains little information to reduce the filesize
	* `PA_list_ECC.csv` uses the same format but only for ECC applications in `PA_triaged.csv`, since only a small proportion of ECC applications are of interest and these are usually well-known or -publicised
	* the ''noted'' status code is used to mark newly identified applications of interest in `PA_list_*.csv` before they are added to `PA_triaged.csv`
* `PA_triaged.csv` contains all applications of interest with more information
	* a number of status codes are used that should be relatively self-explanatory: ''triaged'' means an application has been identified as interesting, ''claimed'' means someone has said he will assemble a response, ''responded'' the response has been submitted (and archived), ''conditional''/''approved''/''withdrawn''/''refused''/''observations'' refer to different decisions, ''reconsult'' is a reconsultation, and ''passed'' is undecided but not responded to
	* each section is sorted by a relevant date: consultation (trigaged, claimed), target (responded), decision (conditional, ...) with latest dates at the top of each section
* `PC_triaged.csv` is like `PA_triaged` but for Planning Consultations (there are many fewer so a `PC_list` is not necessary)

## Automation
* Makefile targets: 
	* `view` opens the above files in a text editor
	* `triage` copies ''noted'' lines in `PA_list_*.csv` to `PA_triaged.csv` with the appropriate formatting, and replaces ''noted'' with ''triaged''

* `src/GenResp` is a shell script that creates text file responses with some information filled in automatically.  See `GenResp -h` for details 
