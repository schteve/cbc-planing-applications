PA_LIST=$(wildcard PA_list_*.csv)
PA_TRIAGED="PA_triaged.csv"
PA_ALL=$(wildcard P?_*.csv)
SEARCH=^PA_list_\([A-Z]*\).csv:\([^,]*,\)\(.*\)\(,[^,]*$$\)
REPLACE=\1,\2TBC,triaged,,,,,\4
STFONT=Liberation Mono:pixelsize=12:antialias=true:autohint=true

.PHONY: triage clean view README

triage:
	grep "noted" $(PA_LIST) | \
	sed "s/$(SEARCH)/$(REPLACE)/" >> $(PA_TRIAGED)
	sed -i 's/noted/triaged/' $(PA_LIST)

clean: 
	@echo "No ./bin to empty"

view:
	$(TERMINAL) -e vim -p $(PA_ALL) &

README:
	$(TERMINAL) -f "$(STFONT)" -e $(MARKDOWN) README.md &

CONTRIBUTING:
	$(TERMINAL) -f "$(STFONT)" -e $(MARKDOWN) CONTRIBUTING.md &
